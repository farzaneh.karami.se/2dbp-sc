import pandas as pd

# Define the file path
file_path = 'dataset_2dbp-half.csv'

# Read the CSV file
df = pd.read_csv(file_path)

# Determine the number of rows in the DataFrame
total_rows = df.shape[0]

# Calculate the number of rows to keep (half of the total)
rows_to_keep = total_rows // 2

# Sample and keep half of the rows
df = df.sample(n=rows_to_keep, random_state=42)  # You can use any random state for reproducibility

# Save the modified DataFrame back to the same file
df.to_csv(file_path, index=False)

print(f"File '{file_path}' has been modified to keep half of the rows.")
