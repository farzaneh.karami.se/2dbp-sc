import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from typing import List
import random
import time

class Item:
    def __init__(self, ID, width, depth, quantity):
        self.ID = ID
        self.width = width
        self.depth = depth
        self.quantity = quantity
        self.x = 0
        self.y = 0
        self.bin = None
        self.color = None  # Added attribute to store color

class Bin:
    def __init__(self, width, depth):
        self.width = width
        self.depth = depth
        self.items = []

def create_items_dict(df):
    return {row['Order_ID']: Item(row['Order_ID'], row['W'], row['L'], row['Quantity']) for _, row in df.iterrows()}

def assign_all_boxes_to_bins(items, bin_width, bin_depth):
    bins = []

    for box_type, item in items.items():
        placed = False

        for bin_obj in bins:
            # Check if the box type is already in the bin
            if any(other_item.ID == box_type and other_item.bin == bin_obj for other_item in bin_obj.items):
                continue

            # Check if the box fits in the bin
            if bin_obj.width - item.x >= item.width and bin_obj.depth - item.y >= item.depth:
                # Check if the box is adjacent to a box of the same type
                same_type_items = [other_item for other_item in bin_obj.items if other_item.ID == box_type]
                if not any(
                    (item.x < other_item.x + other_item.width and
                     item.x + item.width > other_item.x and
                     item.y < other_item.y + other_item.depth and
                     item.y + item.depth > other_item.y)
                    for other_item in same_type_items
                ):
                    # Find a suitable location for the box within the bin
                    locations = [(x, y) for x in range(bin_obj.width - item.width + 1) for y in range(bin_obj.depth - item.depth + 1)]
                    random.shuffle(locations)

                    for x, y in locations:
                        if not any(
                            (x < other_item.x + other_item.width and
                             x + item.width > other_item.x and
                             y < other_item.y + other_item.depth and
                             y + item.depth > other_item.y)
                            for other_item in bin_obj.items
                        ):
                            item.x = x
                            item.y = y
                            item.bin = bin_obj
                            bin_obj.items.append(item)
                            placed = True
                            break

                if placed:
                    break

        if not placed:
            new_bin = Bin(bin_width, bin_depth)
            item.x = random.randint(0, new_bin.width - item.width)
            item.y = random.randint(0, new_bin.depth - item.depth)
            item.bin = new_bin
            new_bin.items.append(item)
            bins.append(new_bin)

    return bins

def check_all_items_assigned(items):
    return all(item.bin is not None for item in items.values())

def report_box_sets(selected_bins: List[Bin]):
    box_count_per_bin = {}

    for i, bin_obj in enumerate(selected_bins):
        box_set = [item.ID for item in bin_obj.items]
        print(f"Bin {i + 1}: {box_set}")

        for item in bin_obj.items:
            if item.ID not in box_count_per_bin:
                box_count_per_bin[item.ID] = 1
            else:
                box_count_per_bin[item.ID] += 1

    print("\nTotal Number of Boxes Assigned to Each Bin:")
    for box_id, count in box_count_per_bin.items():
        print(f"Box ID {box_id}: {count}")

def plot_bins_with_colors_and_sizes(selected_bins: List[Bin]):
    output_directory = "output_plots"
    os.makedirs(output_directory, exist_ok=True)

    for i, bin_obj in enumerate(selected_bins):
        fig, ax = plt.subplots()

        for j, item in enumerate(bin_obj.items):
            color = item.ID  # Use the box ID as the color identifier
            rect = patches.Rectangle(
                (item.x, item.y),
                item.width,
                item.depth,
                linewidth=3,
                edgecolor="black",
                facecolor=f"C{color}",
            )
            ax.add_patch(rect)

        plt.xlim(0, 100)
        plt.ylim(0, 120)
        plt.title(f"Bin Configuration {i + 1}")

        plt.savefig(os.path.join(output_directory, f"bin_plot_{i + 1}.png"))
        plt.close()

def optimize_bins(items, bins, m_percent=20):
    total_bins = len(bins)
    num_bins_to_consider = max(1, int(total_bins * m_percent / 100))

    smallest_bins = sorted(bins, key=lambda bin_obj: len(bin_obj.items))[:num_bins_to_consider]

    while True:
        min_bin = min(smallest_bins, key=lambda bin_obj: len(bin_obj.items))
        min_bin_index = bins.index(min_bin)
        items_to_reassign = min_bin.items

        reassignment_successful = False
        for bin_obj in bins[:min_bin_index] + bins[min_bin_index + 1:]:
            for item in items_to_reassign:
                # Check if the box fits in the bin
                if bin_obj.width - item.x >= item.width and bin_obj.depth - item.y >= item.depth:
                    # Check if the box is adjacent to a box of the same type
                    same_type_items = [other_item for other_item in bin_obj.items if other_item.ID == item.ID]
                    if not any(
                        (item.x < other_item.x + other_item.width and
                         item.x + item.width > other_item.x and
                         item.y < other_item.y + other_item.depth and
                         item.y + item.depth > other_item.y)
                        for other_item in same_type_items
                    ):
                        # Find a suitable location for the box within the bin
                        locations = [(x, y) for x in range(bin_obj.width - item.width + 1) for y in range(bin_obj.depth - item.depth + 1)]
                        random.shuffle(locations)

                        for x, y in locations:
                            if not any(
                                (x < other_item.x + other_item.width and
                                 x + item.width > other_item.x and
                                 y < other_item.y + other_item.depth and
                                 y + item.depth > other_item.y)
                                for other_item in bin_obj.items
                            ):
                                item.x = x
                                item.y = y
                                item.bin = bin_obj
                                bin_obj.items.append(item)
                                reassignment_successful = True
                                break

            if reassignment_successful:
                break

        if not reassignment_successful:
            break

        # Remove items from the min_bin
        min_bin.items = [item for item in min_bin.items if item not in items_to_reassign]

    # Remove bins with no items
    bins = [bin_obj for bin_obj in bins if bin_obj.items]

    return bins

# Example usage
dataset_path = "dataset_2dbp-half.csv"
df = pd.read_csv(dataset_path)
items = create_items_dict(df)

start_time = time.time()
initial_bins = assign_all_boxes_to_bins(items, 100, 120)
optimized_bins = optimize_bins(items.values(), initial_bins, m_percent=30)
end_time = time.time()

unique_box_types = len(set(item.ID for item in items.values()))
print(f"Total Number of Unique Box Types: {unique_box_types}")

num_bins_initial = len(initial_bins)
print(f"Number of Bins (Initial): {num_bins_initial}")

num_bins_optimized = len(optimized_bins)
print(f"Number of Bins (Optimized): {num_bins_optimized}")

all_items_assigned_initial = check_all_items_assigned(items)
print(f"All items assigned (Initial): {all_items_assigned_initial}")

all_items_assigned_optimized = check_all_items_assigned(items)
print(f"All items assigned (Optimized): {all_items_assigned_optimized}")

report_box_sets(optimized_bins)
plot_bins_with_colors_and_sizes(optimized_bins)

print(f"Initial Running Time: {end_time - start_time} seconds")
